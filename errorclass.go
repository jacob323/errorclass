package errorclass

import "fmt"

type Err = error

type WrappedError struct{ Err }

func (e WrappedError) Unwrap() error {
	return e
}

func (e WrappedError) Error() string {
	if e.Err == nil {
		return "(no inner error)"
	}
	return e.Err.Error()
}

type ErrUnexpected WrappedError
type ErrUsage WrappedError
type ErrAbuse WrappedError
type ErrInput WrappedError
type ErrEndUser WrappedError
type ErrClient WrappedError
type ErrExternal WrappedError
type ErrExists WrappedError
type ErrDoesntExist WrappedError
type ErrExhaustedRetries WrappedError
type ErrTODO WrappedError

func (e ErrUnexpected) Error() string  { return fmt.Sprintf("ErrUnexpected: %v", WrappedError(e)) }
func (e ErrUsage) Error() string       { return fmt.Sprintf("ErrUsage: %v", WrappedError(e)) }
func (e ErrAbuse) Error() string       { return fmt.Sprintf("ErrAbuse: %v", WrappedError(e)) }
func (e ErrInput) Error() string       { return fmt.Sprintf("ErrInput: %v", WrappedError(e)) }
func (e ErrEndUser) Error() string     { return fmt.Sprintf("ErrEndUser: %v", WrappedError(e)) }
func (e ErrClient) Error() string      { return fmt.Sprintf("ErrClient: %v", WrappedError(e)) }
func (e ErrExternal) Error() string    { return fmt.Sprintf("ErrExternal: %v", WrappedError(e)) }
func (e ErrExists) Error() string      { return fmt.Sprintf("ErrExists: %v", WrappedError(e)) }
func (e ErrDoesntExist) Error() string { return fmt.Sprintf("ErrDoesntExist: %v", WrappedError(e)) }
func (e ErrExhaustedRetries) Error() string {
	return fmt.Sprintf("ErrExhaustedRetries: %v", WrappedError(e))
}
func (e ErrTODO) Error() string { return fmt.Sprintf("ErrTODO: %v", WrappedError(e)) }
